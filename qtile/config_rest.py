# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#import os
#import re
#import socket
#import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
alt = "mod1"
terminal = guess_terminal()


titletextcolor = "#00ff00"
titlebg = "#252525"
generalbg = "#000000"
layoutbg = "#FFFFFF"
traytextcolor = "#bebebe"


keys = [
    Key([mod, "shift"], "F9", lazy.spawn("systemctl poweroff")),
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_right(),
        desc="Move window down in current stack "),
    Key([mod, "control"], "j", lazy.layout.shuffle_left(),
        desc="Move window up in current stack "),

    # Switch window focus to other pane(s) of stack
    Key([alt], "Tab", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([alt, "shift"], "Tab", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    # Monitor switch
    Key([mod], "h",
        lazy.to_screen(0),
        desc='Keyboard focus to monitor 1'
        ),
    Key([mod], "l",
        lazy.to_screen(1),
        desc='Keyboard focus to monitor 2'
        ),

    #Media Keys
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    #Screenchots
    Key([], "Print", lazy.spawn("flameshot screen -p /home/engell/Imágenes/")),
    Key([mod], "Print", lazy.spawn("flameshot gui")),
    Key(["shift"], "Print", lazy.spawn("flameshot launcher")),
    Key([mod, "shift"], "Print", lazy.spawn("flameshot full -p /home/engell/Imágenes/")),

    Key([mod], "Delete", lazy.spawn("slock")),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([alt], "F4", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key([alt], "F2", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

    # Shortcuts
    Key([mod], "r", lazy.spawn("rofi -show run"), desc="Rofi Launcher"),
    Key([mod], "w", lazy.spawn("rofi -show window"), desc="Rofi window selector"),

    Key([mod], "t", lazy.spawn("telegram-desktop"), desc="Telegram"),
    Key([mod], "c", lazy.spawn("google-chrome"), desc="Chrome"),
    Key([mod], "e", lazy.spawn("thunar"), desc="File Explirer"),
    Key([mod], "v", lazy.spawn("code"), desc="Visual Studio Code"),
    Key([mod], "f", lazy.spawn("firefox"), desc="Firefox"),
    Key([mod], "g", lazy.spawn("gimp"), desc="GIMP"),
    Key([mod], "o", lazy.spawn("libreoffice"), desc="Libre Office"),
    Key([mod], "i", lazy.spawn("inkscape"), desc="Inkscape"),
    Key([mod], "m", lazy.spawn("gnome-system-monitor"), desc="System Monitor"),
    Key([mod], "a", lazy.spawn("atom"), desc="Atom Code Editor"),
]


group_names = '      '.split()
#group_names = '       ﱘ '.split()
groups = [Group(name, layout='monadtall') for name in group_names]
for i, name in enumerate(group_names):
    indx = str(i + 1)
    keys += [
        Key([mod], indx, lazy.group[name].toscreen()),
        Key([mod, 'shift'], indx, lazy.window.togroup(name))]


layout_theme = {"border_width": 3,
                "margin": 6,
                "border_focus": layoutbg, #"90C435",
                "border_normal": titlebg #"384323"
                }


layouts = [
     # Try more layouts by unleashing below layouts.
     #layout.Bsp(),
     #layout.Columns(**layout_theme),
     #layout.Matrix(),
     layout.MonadTall(**layout_theme),
     layout.Max(**layout_theme),
     #layout.MonadWide(),
     #layout.RatioTile(),
     layout.Tile(**layout_theme),
     layout.Stack(num_stacks=2, **layout_theme),
     #layout.TreeTab(
     #    font = "Ubuntu Bold",
     #    fontsize = 10,
     #    section_fontsize = 11,
     #    bg_color = titlebg,
     #    active_bg = layoutbg,
     #    active_fg = generalbg,
     #    inactive_bg = traytextcolor,
     #    inactive_fg = generalbg,
     #    padding_y = 5,
     #    section_top = 10,
     #    panel_width = 250
     #  ),
     #layout.VerticalTile(),
     #layout.Zoomy(**layout_theme),
     #layout.Floating(**layout_theme)

]

widget_defaults = dict(
    font='Ubuntu',
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
				#fontsize=11, 
				font="Costum",
		                fontsize=15,
                                #foreground=titlebg,
				background=generalbg,
				#active=titletextcolor,
				inactive=traytextcolor,
				padding=5, 
				borderwidth=2, 
				urgent_border=titletextcolor,
				disable_drag=True, 
				#highlight_method="block",
				#this_screen_border=titletextcolor,
				#other_screen_border=titletextcolor,
				this_current_screen_border=traytextcolor,				
				#other_current_screen_border=titletextcolor
				),
                widget.Prompt(
				prompt="$",
				font="Ubuntu Mono",
    				fontsize=14,
				foreground=titletextcolor,
				background=generalbg,
				padding=10,
			     ),
                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
		                foreground=generalbg,
				background=titlebg,
				padding = -0,
			      ),
		widget.Sep(
				background=titlebg,
	                        linewidth = 0,
	                        padding = 40,
                          ),

                widget.WindowName(
				font="Ubuntu Bold",
    				fontsize=14,
				foreground=titletextcolor,
				background=titlebg,
				 ),
		
                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
				foreground=layoutbg,
				background=titlebg,
				padding = 0,
			      ),
                widget.Sep(
                        background=layoutbg,
                        linewidth=0,
                        padding=5,
                        ),
                widget.CurrentLayout(
				font="Ubuntu Bold",
				foreground=generalbg,
				background=layoutbg
				    ),
                widget.Sep(
                        background=layoutbg,
                        linewidth=0,
                        padding=5,
                        ),

                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
		                foreground=titlebg,
				background=layoutbg,
				padding = 0,
			      ),
                widget.Systray(
				background=titlebg
			      ),
                widget.Sep(
                        background=titlebg,
                        linewidth=0,
                        padding=10,
                        ),
		widget.TextBox(
                       		text="",
		                font="costum",
                                fontsize=16,
                                foreground=traytextcolor,
				background=titlebg,
		                padding = 5
                        ),
                widget.Volume(
		                foreground=traytextcolor,
				background=titlebg,
		                padding = 0
                        ),
                widget.Sep(
                        background=titlebg,
                        linewidth=0,
                        padding=10,
                        ),
                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
		                foreground=generalbg,
				background=titlebg,
				padding = 0,
			      ),
                widget.Sep(
                        background=generalbg,
                        linewidth=0,
                        padding=5,
                        ),
                widget.Clock(
				font="Ubuntu Bold",
    				fontsize=14,
				foreground=titletextcolor,
				background=generalbg,
				format='%a, %d %B - %H:%M'),
            ], 25,),
    ),
    Screen(
        top=bar.Bar([
            widget.GroupBox(
				#fontsize=11, 
				font="Costum",
		                fontsize=15,
                                #foreground=titlebg,
				background=generalbg,
				#active=titletextcolor,
				inactive=traytextcolor,
				padding=5, 
				borderwidth=2, 
				urgent_border=titletextcolor,
				disable_drag=True, 
				#highlight_method="block",
				#this_screen_border=titletextcolor,
				#other_screen_border=titletextcolor,
				this_current_screen_border=traytextcolor,				
				#other_current_screen_border=titletextcolor
			),
                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
		                foreground=generalbg,
				background=titlebg,
				padding = -0,
			      ),
		widget.Sep(
				background=titlebg,
	                        linewidth = 0,
	                        padding = 40,
                          ),

                widget.WindowName(
				font="Ubuntu Bold",
    				fontsize=14,
				foreground=titletextcolor,
				background=titlebg,
				 ),
		
                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
				foreground=layoutbg,
				background=titlebg,
				padding = 0,
			      ),
                widget.Sep(
                        background=layoutbg,
                        linewidth=0,
                        padding=5,
                        ),
                widget.CurrentLayout(
				font="Ubuntu Bold",
				foreground=generalbg,
				background=layoutbg
				    ),
                widget.Sep(
                        background=layoutbg,
                        linewidth=0,
                        padding=5,
                        ),

                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
		                foreground=titlebg,
				background=layoutbg,
				padding = 0,
			      ),
                widget.Sep(
                        background=titlebg,
                        linewidth=0,
                        padding=10,
                        ),
		widget.TextBox(
                       		text="",
		                font="costum",
                                fontsize=16,
                                foreground=traytextcolor,
				background=titlebg,
		                padding = 5
                        ),
                widget.Volume(
		                foreground=traytextcolor,
				background=titlebg,
		                padding = 0
                        ),
                widget.Sep(
                        background=titlebg,
                        linewidth=0,
                        padding=10,
                        ),
                widget.TextBox("", 
				font="DejaVuSansMono Nerd Font Mono",
    				fontsize=23,
		                foreground=generalbg,
				background=titlebg,
				padding = 0,
			      ),
                widget.Sep(
                        background=generalbg,
                        linewidth=0,
                        padding=5,
                        ),
                widget.Clock(
				font="Ubuntu Bold",
    				fontsize=14,
				foreground=titletextcolor,
				background=generalbg,
				format='%a, %d %B - %H:%M'),
            ], 25),
        )
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"


#@hook.subscribe.startup_once
#def autostart():
#    home = os.path.expanduser('~/.config/qtile/autostart.sh')
#    subprocess.call([home])




# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
