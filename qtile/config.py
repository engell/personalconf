# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import re
import socket
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Screen, Match
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
alt = "mod1"
terminal = guess_terminal()


titletextcolor = "#98971a"
titlebg = "#282828"
generalbg = "#000000"
layoutbg = "#ebdbb2"
traytextcolor = "#bebebe"
current = "#fb4934"
current_this = "#7f7f7f"


keys = [
    # Keys_start

    # Programs:
    Key([mod], "r", lazy.spawn("rofi -show run -font 'Ubuntu Mono 13'"), desc="Rofi Launcher"),
    Key([mod], "w", lazy.spawn("rofi -show window -font 'Ubuntu Mono 13'"), desc="Rofi window selector"),
    Key([mod], "p", lazy.spawn("/home/engell/.personalconf/scripts/bwmenu.sh -c 20"), desc="Bw Menu"),
    Key([mod], "t", lazy.spawn("telegram-desktop"), lazy.spawn("notify-send 'Abriendo: Telegram'"), desc="Telegram"),
    Key([mod], "b", lazy.spawn("brave-browser"), lazy.spawn("notify-send 'Abriendo: Navegador'"), desc="Web browser"),
    Key([mod], "c", lazy.spawn("google-chrome"), lazy.spawn("notify-send 'Abriendo: Chrome'"), desc="Chrome"),
    Key([mod], "f", lazy.spawn("firefox"), lazy.spawn("notify-send 'Abriendo: Firefox'"), desc="Firefox"),
    Key([mod], "e", lazy.spawn("thunar"), lazy.spawn("notify-send 'Abriendo: Explorador'"), desc="File Explirer"),
    Key([mod], "s", lazy.spawn("spotify"), lazy.spawn("notify-send 'Abriendo: Spotify'"), desc="Spotify"),
    Key([mod], "m", lazy.spawn("gnome-system-monitor"), lazy.spawn("notify-send 'Abriendo: System-Monitor'"), desc="System Monitor"),

    # Window Control:
    Key([alt], "Tab", lazy.layout.next(), desc="Switch window focus"),
    Key([alt], "F4", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "k", lazy.layout.down(), desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(), desc="Move focus up in stack pane"),
    Key([mod, "control"], "k", lazy.layout.shuffle_right(), desc="Move window down in current stack "),
    Key([mod, "control"], "j", lazy.layout.shuffle_left(), desc="Move window up in current stack "),

    # Qtile control:
    Key([mod], "F1", lazy.spawn("/home/engell/.personalconf/scripts/qtile_keybindings.sh"), desc="Show all the key bindings"),
### Key([mod], "1-9", lazy.spaw(), desc="Go to [1-9] desktop area"),
### Key([mod, "shift"], "1-9", lazy.spaw(), desc="Move window to [1-9] desktop area"),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile [No exit]"),
    Key([alt, "control"], "BackSpace", lazy.shutdown(), desc="Exit qtile"),
    Key([alt], "F2", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "F9", lazy.spawn("systemctl poweroff"), lazy.spawn("numlockx off"), desc="System shutdown"),
    Key([mod], "Delete", lazy.spawn("slock"), desc="Lock screen"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([alt, "shift"], "Tab", lazy.layout.rotate(), desc="Swap panes of split stack"),
    Key(["control"], "Escape", lazy.spawn("/home/engell/.personalconf/scripts/xmenu.sh"), desc="Run Xmenu"),

    # Keyboard Layouts:
    Key([mod], "F12", lazy.spawn("/home/engell/.personalconf/scripts/keyboardRemap.sh dvorak"), desc="Dvorak Layout"),
    Key([mod], "F11", lazy.spawn("/home/engell/.personalconf/scripts/keyboardRemap.sh devel"), desc="Dvelop Layout"),
    Key([mod], "F10", lazy.spawn("/home/engell/.personalconf/scripts/keyboardRemap.sh qwerty"), desc="Qwerty Layout"),

    # Monitor switch:
    Key([mod, "shift"], "period", lazy.to_screen(0), desc="Keyboard focus primary Monitor"),
    Key([mod, "shift"], "comma", lazy.to_screen(2), desc="Keyboard focus secundary Monitor"),
    Key([mod, "shift"], "ntilde", lazy.to_screen(1), desc="Keyboard focus system Monitor"),

    # Screenchots:
    Key([], "Print", lazy.spawn("flameshot screen -p /home/engell/Imágenes/"), desc="Screenshot of current monitor"),
    Key([mod], "Print", lazy.spawn("flameshot gui"), desc="Select region to screenshot"),
    Key(["shift"], "Print", lazy.spawn("flameshot launcher"), desc="Launch screenshot aplication"),
    Key([mod, "shift"], "Print", lazy.spawn("flameshot full -p /home/engell/Imágenes/"), desc="Screenshot to all monitors"),
    Key([alt], "Print", lazy.spawn("/home/engell/.personalconf/scripts/window_screenshot.sh"), desc="Screenshot to current window"),

    # Media Keys:
    Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc="Player next song"),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous"), desc="Player previous song"),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause"), desc="Player play/pause"),

    # Keys_end
]


social = Match(wm_class=["telegram-desktop"])
www = Match(wm_class=["google-chrome", "Firefox-esr"])

group_names = [
    ("", {'layout': 'max', 'spawn': 'brave-browser', 'matches': www}),
    ("", {'layout': 'max'}),
    ("", {'layout': 'max'}),
    ("", {'layout': 'monadtall'}),
    ("", {'layout': 'max'}),
    ("", {'layout': 'max', 'matches': social}),
    ("", {'layout': 'max'}),
    ("", {'layout': 'monadtall'}),
    ("", {'layout': 'max'})
    ]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group


layout_theme = {"border_width": 3,
                "margin": 6,
                "border_focus": titletextcolor, 
                "border_normal": titlebg, 
                }


layouts = [
     layout.MonadTall(**layout_theme),
     layout.Max(**layout_theme),
     layout.Tile(**layout_theme),
     layout.Stack(num_stacks=2, **layout_theme),
]

widget_defaults = dict(
    font='Ubuntu',
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        widget.GroupBox(    #0
            #fontsize=11, 
            font="Costum",
            fontsize=20,
            #fontshadow=layoutbg,
            #foreground=titlebg,
            background=generalbg,
            active=traytextcolor,
            inactive=titlebg,
            block_highlight_text_color=layoutbg,
            padding=5, 
            borderwidth=2, 
            urgent_alert_method="text",
            urgent_border=titletextcolor,
            disable_drag=True, 
            highlight_method="line",
            highlight_color=[titlebg, current_this],
            this_screen_border=traytextcolor,
            other_screen_border=traytextcolor,
            this_current_screen_border=titletextcolor,
            other_current_screen_border=current,
            rounded=True
            ),
        widget.Prompt(  #1
            prompt="$",
            font="Ubuntu Mono",
            fontsize=14,
	    foreground=titletextcolor,
	    background=generalbg,
	    padding=10,
	    ),
        widget.WidgetBox( #2
	    font="DejaVuSansMono Nerd Font Mono",
            fontsize=20,
            foreground=current_this,
            text_closed="°",
            text_open="°",
            widgets=[
                widget.Pomodoro(
	            font="DejaVuSansMono Nerd Font Mono",
                    length_pomodori=40,
                    length_short_break=5,
                    length_long_break=15,
                    fontsize=12,
                    notification_on=True,
                    prefix_inactive="engell.me",
                    prefix_paused="engell.me",
                    background=generalbg,
                    foreground=current,
                    ),
                widget.TextBox(
	            font="DejaVuSansMono Nerd Font Mono",
                    fontsize=20,
                    foreground=current_this,
                    text="°"
                    ),
                ],
            ),
        widget.TextBox( #3
            "", 
            font="DejaVuSansMono Nerd Font Mono",
            fontsize=20,
            foreground=generalbg,
	    background=titlebg,
	    padding = -0,
	    ),
        # widget.Wttr(
        #     lang='es',
        #     location={
        #                'Minsk': 'Minsk',
        #                '64.127146,-21.873472': 'Reykjavik',
        #            },
        #     format='%l: %C, temp: %t, feels: %f',
        #     units='m',
        #     update_interval=30,
        #     ),
        widget.Sep( #4
	    background=titlebg,
	    linewidth = 0,
	    padding = 40,
            ),
        widget.WindowName( #5
	    font="Ubuntu Bold",
    	    fontsize=14,
	    foreground=titletextcolor,
	    background=titlebg,
	    ),
        widget.Notify( #6
	    foreground=titletextcolor,
	    background=titlebg,
            font="Ubuntu Mono",
            default_timeout=10,
            max_chars=15,
            padding = 5,
            ),
        widget.TextBox( #7
            "", 
	    font="DejaVuSansMono Nerd Font Mono",
    	    fontsize=20,
	    foreground=layoutbg,
	    background=titlebg,
	    padding = 0,
	    ),
        widget.Sep( #8
            background=layoutbg,
            linewidth=0,
            padding=5,
            ),
        widget.CurrentLayout( #9
	    font="Ubuntu Bold",
	    foreground=generalbg,
	    background=layoutbg
	    ),
        widget.Sep( #10
            background=layoutbg,
            linewidth=0,
            padding=5,
            ),
        widget.TextBox( #11
            "", 
	    font="DejaVuSansMono Nerd Font Mono",
    	    fontsize=20,
	    foreground=titlebg,
	    background=layoutbg,
	    padding = 0,
	    ),
        widget.Systray( #12
	    background=titlebg,
            padding=7,
            icon_size=20,
	    ),
        widget.Sep( #13
            background=titlebg,
            linewidth=0,
            padding=10,
            ),
	widget.TextBox( #14
            text="",
	    font="costum",
            fontsize=16,
            foreground=traytextcolor,
	    background=titlebg,
	    padding = 5,
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('/home/engell/.personalconf/scripts/qtile_keybindings.sh')}
            ),
        widget.Volume( #15
	    foreground=traytextcolor,
	    background=titlebg,
	    padding = 0
            ),
        widget.KeyboardLayout( #16
            background=titlebg,
            foreground=traytextcolor,
            configured_keyboards=["latam dvorak", "latam", "us dvp"],
            display_map={"latam dvorak": "Ñ-Dvk", "latam": "Ñ", "us dvp": "dDev"},
            padding=10
            ),
        widget.Sep( #17
            background=titlebg,
            linewidth=0,
            padding=5,
            ),
        widget.TextBox( #18
            "", 
	    font="DejaVuSansMono Nerd Font Mono",
    	    fontsize=20,
	    foreground=generalbg,
	    background=titlebg,
	    padding = 0,
	    ),
        widget.Sep( #19
            background=generalbg,
            linewidth=0,
            padding=5,
            ),
        widget.Clock( #20
	    font="Ubuntu Bold",
    	    fontsize=14,
	    foreground=titletextcolor,
	    background=generalbg,
	    padding = 5,
	    format='%a, %d %B - %H:%M'),
        widget.TextBox( #21
            text="", 
	    font="DejaVuSansMono Nerd Font Mono",
    	    fontsize=30,
	    foreground=layoutbg,
	    background=generalbg,
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('/home/engell/.personalconf/scripts/xmenu.sh')},
	    padding = 5,
	    ),
    ]
    return widgets_list
	
	
def init_widgets_screen1():
    widgets_screen = init_widgets_list()
    return widgets_screen
	
def init_widgets_screen2():
    widgets_screen = init_widgets_list()
    #widgets_screen.pop(15)
    widgets_screen.pop(15) # Volume widget
    widgets_screen.pop(14) # Volume icon widget
    widgets_screen.pop(13) # Separator widget
    widgets_screen.pop(12) # Systray widget
    widgets_screen.pop(2)  # WidgetBox
    widgets_screen.pop(1)  # Promp Widget
    #widgets_screen.pop(13)
    return widgets_screen
	
def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=0.8, size=25)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=0.8, size=25)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=0.8, size=25))]
		
		
if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
#        widgets_screen2 = init_widgets_screen2()


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.toggle_fullscreen())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    border_focus = titletextcolor,
    border_normal = titlebg,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        {'wmclass': 'confirm'},
        {'wmclass': 'dialog'},
        {'wmclass': 'download'},
        {'wmclass': 'error'},
        {'wmclass': 'file_progress'},
        {'wmclass': 'notification'},
        {'wmclass': 'splash'},
        {'wmclass': 'toolbar'},
        {'wmclass': 'confirmreset'},  # gitk
        {'wmclass': 'makebranch'},  # gitk
        {'wmclass': 'maketag'},  # gitk
        {'wname': 'branchdialog'},  # gitk
        {'wname': 'pinentry'},  # GPG key password entry
        {'wmclass': 'ssh-askpass'},  # ssh-askpass
        {'wname': 'VirtualBox - Question'},  # ssh-askpass
        {'wmclass': 'yad'},  # yad notify
        {'wmclass': 'gkbd-keyboard-display'},  # gtk-keyboard
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.personalconf/scripts/autostart.sh')
    subprocess.call([home])




# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "qtile"
