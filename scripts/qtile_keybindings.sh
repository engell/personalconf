#!/usr/bin/bash

notify-send 'Mostrando: Accesos directos'
file=~/.personalconf/qtile/config.py

sed -n "/Keys_start/,/Keys_end/p" $file | \
  grep -v "# Keys" | \
  sed -e "s/###//" \
  -e "s/^[ \t]*//" \
  -e "/^ *$/d" \
  -e "s/# /    /" \
  -e "s/Key(\[mod\],/Super +/" \
  -e "s/Key(\[mod,/Super +/" \
  -e "s/Key(\[alt\],/Alt +/" \
  -e "s/Key(\[alt,/Alt +/" \
  -e "s/Key(\[\"shift\"\],/Shift +/" \
  -e "s/Key(\[\"control\"\],/Ctrl +/" \
  -e "s/Key(\[\], \"//" \
  -e "s/ \"control\"], / Ctrl + /" \
  -e "s/ \"shift\"], / Shift + /" \
  -e "s/ + \"/ + /" \
  -e "s/\", /\t/" \
  -e "s/desc=\"/\t/" | \
  cut -f 1,3 | \
  sed -e "s/\"),//" \
  -e "s/\t/ . . . . . . . . . . /" | \
  yad --text-info --title="Key Bindings" --back=#282828 --fore=#98971a --borders=0 --geometry=600x870 --no-buttons --on-top --skip-taskbar --close-on-unfocus --fontname="Ubuntu mono 12"
