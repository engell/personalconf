#!/bin/bash
#gtf 1280 800 60
#xrandr --newmode "1280x800_60.00"  83.46  1280 1344 1480 1680  800 801 804 828  -HSync Vsync
#xrandr --addmode DVI-I-0 1280x800_60.00 
#xrandr --output DVI-I-0 --mode 1280x800_60.00 --right-of HDMI-0
#x11vnc -forever -bg -viewonly -allow 192.168.0.4 -deferupdate 60 -clip 1280x800+3520+0
#x11vnc -clip 1280x800+3520+0 > foo.out 2> foo.err < /dev/null & 

while :
do
  if pgrep x11vnc > /dev/null
  then
    echo running
  else
    # x11vnc -forever -bg -viewonly -allow 192.168.0.4 -deferupdate 60 -clip 1280x800+3520+0 
    x11vnc -forever -bg -viewonly -deferupdate 60 -clip 1280x800+3520+0 
  fi
  sleep 5
done
