#!/bin/bash

d=$1
if [ $d == "qwerty" ]; then
  notify-send "Qwerty 4 the world"
  feh --bg-scale ~/.personalconf/wallpapers/wallpaper.jpg
  setxkbmap latam
elif [ $d == "dvorak" ]; then
  notify-send "Dvorak Rules"
  setxkbmap latam -variant dvorak
  xmodmap -e "keycode 24 = period colon period colon less at"
  xmodmap -e "keycode 52 = minus underscore minus underscore greater macron"
  feh --bg-scale ~/.personalconf/wallpapers/wallpaper.jpg
elif [ $d == "devel" ]; then
  notify-send "Developer Rules"
  feh --bg-scale ~/.personalconf/wallpapers/wallpaper-devel.jpg 
  setxkbmap us -variant dvp
else
  notify-send "Nadien Rules"
fi

xmodmap -e "clear Lock"
xmodmap -e "keycode 66 = Escape"
xmodmap -e "keycode 121 = Caps_Lock"
xmodmap -e "keycode 123 = Down"
xmodmap -e "keycode 122 = Up"

if pgrep superkb > /dev/null
then
  killall superkb 
fi

sleep 1
superkb &
