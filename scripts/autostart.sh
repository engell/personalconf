#!/bin/bash

### Monitor virtual al inicio ###
#screen -dmS vMonitor ~/.personalconf/scripts/virtualmonitor.sh 
### Aplicaciones al inicio ###
#sleep 2
feh --bg-scale ~/.personalconf/wallpapers/wallpaper.jpg &
synapse -s &
# conky -b &
picom &
#redshift-gtk &
### Activar Num Lock ###
numlockx on &
### Remapear Teclado ###
~/.personalconf/scripts/keyboardRemap.sh dvorak #&
imwheel
### Desactivar Apagado automático del monitor ###
xset dpms 0 0 0
xset s  reset
xset s  off
### Aliases ###

