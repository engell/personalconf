#!/bin/bash

width="$1"
height="$2"

for file in *
do
  archivo=`echo $file`
  salida=`echo $file | cut -d '.' -f1`
  final=$salida"_min.webp"
  cwebp $archivo -resize $width $height -o $final
done

echo $archivo -resize $width $height -o $final

exit
