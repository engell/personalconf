#!/bin/bash


for file in *
do
  archivo=`echo $file`
  salida=`echo $file | cut -d '.' -f1`
  final=$salida".webp"
  cwebp $archivo -lossless -o $final
done

echo $archivo -o $final

exit
