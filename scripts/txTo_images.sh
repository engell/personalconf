#!/bin/bash

id="$1"
img="$2"
string="$3"
font="$4"
size="$5"
color="$6"
align="$7"
posx="$8"
posy="$9"

ext=`echo "$img" | cut -d'.' -f2`

convert "$img" \
	-gravity $align \
	-font "$font" \
	-pointsize "$size" \
	-fill "#$color" \
	-draw "text $posx,$posy '$string'" \
	$id"."$ext
