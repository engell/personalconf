#!/bin/bash

overide=$1
text="$2"
font="$3"
size="$4"
color="$5"
align="$6"
posx="$7"
posy="$8"

if [$overide=1]; then
  echo ""
else
  echo ""
fi

for file in *
do
  archivo=`echo $file`
  salida=`echo $file | cut -d '.' -f1`
  ~/.personalconf/scripts/txTo_images.sh $salida $archivo $text $font $size $color $align $posx $posy
done

echo $salida $archivo $text $font $size $color $align $posx $posy

exit
