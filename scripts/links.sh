#!/bin/bash

# Dotfiles
mv ~/.gitconfig ~/.gitconfig_old
mv ~/.tmux.conf ~/.tmux.conf_old
mv ~/.vimrc ~/.vimrc_old
mv ~/.conkyrc ~/.conkyrc_old
mv ~/.screenrc ~/.screenrc_old
mv ~/.imwheelrc ~/.imwheelrc_old
ln -s ~/.personalconf/dotfiles/gitconfig ~/.gitconfig
ln -s ~/.personalconf/dotfiles/tmux.conf ~/.tmux.conf
ln -s ~/.personalconf/dotfiles/vimrc ~/.vimrc
ln -s ~/.personalconf/dotfiles/conkyrc ~/.conkyrc
ln -s ~/.personalconf/dotfiles/screenrc ~/.screenrc
ln -s ~/.personalconf/dotfiles/imwheelrc ~/.imwheelrc


# Folders
mv ~/.config/nvim ~/.config/nvim_old
mv ~/.cinfig/qtile ~/.config/qtile_old
mv ~/.config/alacritty ~/.config/alacritty_old
mv ~/.config/rofi ~/.config/rofi_old
mkdir ~/.local/share/fonts
ln -s ~/.personalconf/fonts ~/.local/share/fonts/personal
ln -s ~/.personalconf/nvim ~/.config/nvim
ln -s ~/.personalconf/qtile ~/.config/qtile
ln -s ~/.personalconf/alacritty ~/.config/alacritty
ln -s ~/.personalconf/rofi ~/.config/rofi
