# Add your own custom alias in the custom/aliases directory. Aliases placed
# here will override ones with the same name in the main alias directory.

alias v="nvim"
alias status="tmux split-window -v -p 85 'bashtop' ; tmux select-pane -t 1 ; tmux split-window -h -p 18 'tty-clock -B' ; tmux select-pane -t 1"
alias sp="~/.personalconf/scripts/tmuxSplit.sh"
