# Personal Conf

Repositorio de configuración de [Engell](https://twitter.com/engell25)

Este repositorio está creado con el fin de guardar la configuración de algunas aplicaciones para reducir el trabajo al momento de formatear o cambiar de equipo.

Los archivos de configuración corresponden a:

- git
- tmux
- vim y nvim
- conky
- qtile

También incluye algunas fuentes y wallpapers.

```
sh -c "$(curl -fsSL https://gitlab.com/engell/personalconf/raw/master/scripts/install.sh)"
```

>[![Licencia Creative Commons](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es)
Repositorio creado por [Ángel Isaac Pizano](https://engell.me) se encuentra bajo una [Licencia Creative Commons Atribución-NoComercial-CompartirIgual 3.0 Unported](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es).
