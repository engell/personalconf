Configs a modificar

Install:

  sudo apt install neovim tmux conky feh synapse rofi


Oh-my-Bash configs:
  
  sh -c "$(curl -fsSL https://raw.github.com/ohmybash/oh-my-bash/master/tools/install.sh)"

  ~/.bashrc:
    SH_THEME="powerline"
    export PATH=$PATH:/usr/local/go/bin

  ~/.oh-my-bash/themes/powerline/powerline.theme.sh
    USER_INFO_THEME_PROMPT_COLOR=22
    SCM_THEME_PROMPT_CLEAN_COLOR=28


Neovim:

  Plug:

    sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
         https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

  Autocompletado:

    sudo apt install -y vim-youcompleteme

  Diccionarios:

    mkdir -p ~/.vim/spell
    cd ~/.vim/spell
    wget http://ftp.vim.org/vim/runtime/spell/es.latin1.spl
    wget http://ftp.vim.org/vim/runtime/spell/es.latin1.sug
    wget http://ftp.vim.org/vim/runtime/spell/es.utf-8.spl
    wget http://ftp.vim.org/vim/runtime/spell/es.utf-8.sug
